<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'mhd' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         ']dpG%4{d`zZ5H1F^ Pn_S@8(2Z0PctHO6f`Q~qRjEL*%q$W)E5VsHwZR@[fl-:bh' );
define( 'SECURE_AUTH_KEY',  'LRGuXm:CXp{E-.>Hoq/E|WeT`]cniVh S4raRyJx*2.Prmpg3t&e^WV7D7:U&v_P' );
define( 'LOGGED_IN_KEY',    '?j7EbI[smkalA=bDZ-WF_9 d}zhp{r|$y*/#%k<3UTOt*JpggN{ `5EhEPNp{wEp' );
define( 'NONCE_KEY',        'VLz&A,K1Z7#ALtGiF,NegR03Tap^*?VvVKL6XP#lmPgf31&^>1d3BO0W)k%wY-;l' );
define( 'AUTH_SALT',        '0$TmM|YO4/V>KL2Lzo<QXeGD:?hu[-=:wQ)le/7!g_mS288<aj-?7k/8I}|q*!ck' );
define( 'SECURE_AUTH_SALT', 'ETkwBPOBLw$T jF]>h(+z)Cqva3&acFN|hi?Zsd+{g<l<8w&U,dZSQkZ@{K%>>@w' );
define( 'LOGGED_IN_SALT',   'H];i+,`Qn1N;{h;B&X*AxuXP|!FF~:Gk9R8 B_,cXa(@=iM|E<NGX67J:X_v-von' );
define( 'NONCE_SALT',       ',)x8aI&*7N95ct~,,_obp(zS*@jlX+Y>S%s?S>r/pg_IYqJ* 9S&JOOZ/CV7)Hy!' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
